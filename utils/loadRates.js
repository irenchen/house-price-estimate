const fs = require('fs')
const csv = require('csv-parser')

var counter = 0;
let rates = []
fs.createReadStream(`./doc/rates.csv`)
    .pipe(csv())
    .on('data', data => {
        counter++
        // console.log(data['地區'])
        rates.push(data)
    })
    .on('end', () => {
        console.log('parse csv file end. total count = ', counter)
        console.log(rates.length)
        fs.writeFileSync('./utils/rates.json', JSON.stringify(rates))
    })

