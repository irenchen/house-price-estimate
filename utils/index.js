const config = require('../config')
const googleMapsClient = require('@google/maps').createClient({
   key: config.googleMapsKey
});

const fs = require('fs')
let RATES = fs.readFileSync('./utils/rates.json', 'utf8')
RATES = JSON.parse(RATES)
const DB = require('../db')
const PLATEAU = 3.3058 // 1 plateau = 3.3058 square meter
const moment = require('moment')

// googleMapsClient.geocode({
//    address: '臺北市文山區指南路一段一號'
//  }, function(err, response) {
//    if (!err) {
//      console.log(JSON.stringify(response.json.results, null, 2));
//    }
// });

module.exports = {
    async findCoordinateByAddress (address) {
        return new Promise((resolve, reject) => {
            googleMapsClient.geocode({
                address
            }, (err, response) => {
                if(err) return reject(err)
                if(response.json.results.length == 0) return reject('wrong input...')
                resolve(response.json.results[0].geometry.location)
            })
        })
    },
    async findPriceByCoordinateAndDistrict (coordinate, district) {
        return new Promise(async (resolve, reject) => {
            try {
                // const results = await DB.getNearbyHousesByCoordinateAndDistrict(coordinate, district)
                const results = await DB.getNearbyHouses(coordinate, district)
                if (results.length >= config.numberToEstimate) {
                    let processed = results.slice(0, config.numberToEstimate).map(house => {
                        return house.totalPrice / house.totalArea
                    })
                    let sum = processed.reduce((sum, element) => {
                        return sum += element
                    }, 0)
                    let avg = sum / config.numberToEstimate
                    console.log(`avg price = ${avg} per meter square`)
                    resolve(avg)
                } else {
                    const result2 = await DB.getNearbyHouses(coordinate, district, config.maxDistanceRetry)
                    if(result2.length >= config.numberToEstimate) {
                        let processed = result2.slice(0, config.numberToEstimate).map(house => {
                            return house.totalPrice / house.totalArea
                        })
                        let sum = processed.reduce((sum, element) => {
                            return sum += element
                        }, 0)
                        let avg = sum / config.numberToEstimate
                        console.log(`avg price = ${avg} per meter square`)
                        resolve(avg)
                    } else if(result2.length > 0) {
                        let processed = result2.map(house => {
                            return house.totalPrice / house.totalArea
                        })
                        let sum = processed.reduce((sum, element) => {
                            return sum += element
                        }, 0)
                        let avg = sum / result2.length
                        console.log(`avg price = ${avg} per meter square`)
                        resolve(avg) 
                    } else {
                        resolve(0)
                    }                   
                }
            } catch (e) {
                console.log(e)
                reject(e)
            }            
        })
    },
    async getNearbyHouses (coordinate, district) {
        return new Promise(async (resolve, reject) => {
            try {
                const results = await DB.getNearbyHousesByCoordinateAndDistrict(coordinate, district)
                resolve(results.slice(0, config.numberToEstimate))
            } catch (e) {
                console.log(e)
                reject(e)
            }
        })
    },
    getWeight ({ age, floor, houseType, hasTopOver }) {
        let weight = 1
        weight *= evaluateAge(age)
        weight *= evaluateFloor(floor, houseType, hasTopOver)
        console.log(`weight = ${weight}`)
        return weight
    },
    getPossibleLoan (estimatedPrice, city, district) {
        let rate = RATES.find(r => r['城市'] == city && r['地區'] == district) || { '債權商城貸款成數': 0.8 }
        console.log(rate)
        return Math.floor(estimatedPrice * rate['債權商城貸款成數'])
    },
    async verifyIp (ip) {
        return new Promise(async (resolve, reject) => {
            try {
                const visitor = await DB.getVisitorByIp(ip)
                console.log(`now : ${new Date().toLocaleDateString()}`)
                console.log(`last : ${visitor.visitTime.toLocaleDateString()}`)
                if (visitor.count < config.maxVisitsPerDay) {
                    // increment ip counter
                    visitor.count++
                    visitor.visitTime = new Date()
                    await DB.updateVisitorByIp(visitor)
                    return resolve('ok')
                } else {
                    let now = new Date().toLocaleDateString()
                    let last = visitor.visitTime.toLocaleDateString()
                    if (moment(now).isAfter(last)) {
                        // reset ip counter
                        visitor.count = 1
                        visitor.visitTime = new Date()
                        await DB.updateVisitorByIp(visitor)
                        resolve('ok')
                    } else {
                        resolve('over')
                    }
                }
            } catch (err) {
                reject(err)
                console.log(err)
            }
        })        
    }
}

function evaluateAge (age) {
    age = parseFloat(age)
    if (age <= 2) {
        return 1.2
    } else if (age <= 5) {
        return 1.15
    } else if (age <=10) {
        return 1.1
    } else if (age <= 20) {
        return 1.05
    } else if (age <= 30) {
        return 1.0
    } else if (age <= 40) {
        return 0.975
    } else if (age <= 50) {
        return 0.95
    } else if (age <= 60) {
        return 0.925
    } else {
        return 0.9
    }
}

// houseType 1 : 套房
// houseType 2 : 公寓
// houseType 3 : 華廈(10層含以下有電梯)
// houseType 4 : 住宅大樓(11層含以上有電梯)
// houseType 5 : 透天
function evaluateFloor(floor, houseType, hasTopOver) {
    floor = parseInt(floor)
    if (houseType == 2) { // apartment
        if (floor == 1) return 1.25
        if (floor == 2 || floor == 3 || hasTopOver) return 1.0
        if (floor == 4) return 0.85
        if (floor == 5) return 0.85
    } else if (houseType == 1 || houseType == 3 || houseType == 4) {
        if (floor == 1) return 1.05
        if (floor >= 2 && floor <= 4) return 0.9
        if (floor >= 5 && floor <= 10) return 1.0
        if (floor >= 11 && floor <= 19) return 1.025
        if (floor >= 20) return 1.05
    } else {
        return 1.0
    }
}
