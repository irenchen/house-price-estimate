module.exports = {
  dbUri: 'mongodb://192.168.56.100:27017/lmarket',
  googleMapsKey: 'AIzaSyC94g6TTVI0n8QUFpBwsPIUN-_emgbkHpk',
  maxVisitsPerDay: 20, // 預設的限制ip每天查詢上限
  maxDistance: 500, // 預設的搜尋範圍半徑
  maxDistanceRetry: 1000, // 不足十筆資料時，放寬搜尋區域範圍
  numberToEstimate: 10, // 只用前面十筆資料做計算
}
