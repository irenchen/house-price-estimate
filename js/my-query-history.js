const componentMyQueryHistory = Vue.component('my-query-history', {
    template: `
        <div class="container">
            <div class="row" style="width:80%;margin:0 auto;padding-top:50px;">
                <p style="display:flex; justify-content:space-between;">
                    <span style="font-size:1.1em;">試算紀錄</span>
                    <button class="btn btn-default" 
                            @click="handleClearHistory"
                            v-if="items.length > 0">
                        清除紀錄
                    </button>
                </p>
                <p style="border:0.5px solid #999;"></p>
            </div>
            <div class="row" v-if="items.length > 0">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <table class="table table-hover">
                        <thead>
                            <tr><th>#</th><th>地址</th><th>單價(萬元/坪)</th><th>總價(萬元)</th><th>可貸金額(萬元)</th></tr>
                        </thead>
                        <tbody>
                            <tr v-for="(h, index) in items" 
                                :key="index"
                                @click="handleHistoryClick(h)">
                                <td>{{ index + 1 }}</td>
                                <td>{{ h.result.addr }} {{ h.data.floor ? h.data.floor + '樓' : ''}}</td>
                                <td>{{ h.result.avg }}</td>
                                <td>{{ h.result.estimatedPrice | myPriceFilter }}</td>
                                <td>{{ h.result.possibleLoan | myPriceFilter }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    `,
    props: ['history'],
    data: function() {
        return {

        }
    },
    computed: {
        items () {
            return this.history.map(h => JSON.parse(h)).reverse()
        }
    },
    watch: {

    },
    methods: {
        handleClearHistory() {
            this.$emit('clear-history')
        },
        handleHistoryClick(h) {
            this.$emit('history-click', h)
        }
    },
    created() {

    },
    mounted() {

    }

})


