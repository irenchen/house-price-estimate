const componentMyFooter = Vue.component('my-footer', {
    template: `
        <div>
            <footer class="main" id="main-footer">           
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <p>Copyright © {{ new Date().getFullYear() }}, Lmarket, All Rights Reserved</p>
                            </div>
                            <div class="col-md-2">
                                <a href="https://lmarket.com.tw/mobile-serviceTerms.action">
                                    <span>服務條款</span>
                                </a>
                                <a href="https://lmarket.com.tw/mobile-contus.action">
                                    <span style="margin-left:20px;">聯絡我們</span>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <div class="pull-right">
                                    <div style="width:171px; float:left;">
                                        <img src="img/logo.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    `,
    props: [],
    data: function() {
        return {

        }
    },
    computed: {

    },
    watch: {

    },
    methods: {

    },
    created() {

    },
    mounted() {

    }

})


