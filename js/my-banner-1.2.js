const componentMyBanner1 = Vue.component('my-banner-1', {
    template: `
        <div class="row" style="margin: 0;">
            <div class="col-sm-9 col-sm-offset-1">
                <div id="slick">
                    <h1 class="banner1-h1">
                        <a href="loan_demo.html" target="_blank"
                            class="btn banner1-h1-btn1 hidden">
                            02-2706-0066
                        </a>
                        <a href="loan_demo.html" target="_blank"
                            class="btn banner1-h1-btn2 hidden">
                            @liq9224x
                        </a>
                    </h1>
                    <h1 class="banner1-h2" >
                        <a href="loan_demo.html" target="_blank" 
                            class="btn banner1-h2-btn1 hidden">
                            了解更多 >
                        </a>
                    </h1>
                    <h1 class="banner1-h3" >
                        <a href="loan_demo.html" target="_blank" 
                            class="btn banner1-h3-btn1 hidden">
                            了解更多 >
                        </a>
                    </h1>
                </div>
            </div>
        </div>
    `,
    props: [],
    data: function() {
        return {
            screenWidth: window.innerWidth,
        }
    },
    computed: {
        // bannerStyle() {
        //     return {
        //         height: Math.floor(this.screenWidth / 4) + 'px'
        //     }
        // },
        // bannerHeight() {
        //     return Math.floor(this.screenWidth / 4) + 'px'
        // }
    },
    watch: {

    },
    methods: {
        handleResize(evt) {
            // console.log(`window resize to ${window.innerWidth}`)
            this.screenWidth = window.innerWidth
        }
    },
    created() {
        window.addEventListener('resize', this.handleResize)
    },
    mounted() {
        $(function() {
            $('#slick')
                .slick({
                    autoplay: true,
                    autoplaySpeed: 2000,
                    pauseOnFocus: true,
                    pauseOnHover: true,
                    dots: true,
                    arrows: false,
                })
        })

    }

})


