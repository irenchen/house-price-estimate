const componentMyLoanDemo = Vue.component('my-loan-demo', {
    template: `
        <div class="container">
            <div class="row">
                <div style="padding: 0 80px;">
                    <h4>一、產品介紹</h4>
                    <p style="font-size:1.1em;margin-left:35px;">「L market 債權商城」是指申請人提供個人名下不動產作為抵押擔保，透過日昇金互聯網股份有限公司媒合天使投資人所申請的短期快速不動產貸款業務。</p>
                </div>
            </div>
            <div class="row">
                <div style="padding: 0 80px;">
                    <h4>二、適用人群</h4>
                    <p style="font-size:1.1em;margin-left:35px;">個人名下擁有不動產即可申請。</p>
                </div>
            </div>
            <div class="row">
                <div style="padding: 0 80px;">
                    <h4>三、產品優勢</h4>
                    <p style="font-size:1.1em;margin-left:35px;">1. 利率優惠，月息最低0.5%;</p>
                    <p style="font-size:1.1em;margin-left:35px;">2. 手續簡便、輕鬆辦理;</p>
                    <p style="font-size:1.1em;margin-left:35px;">3. 額度高，最高可達抵押房產估值的80%;</p>
                    <p style="font-size:1.1em;margin-left:35px;">4. 放款時間快，最快3天內放款;</p>
                    <p style="font-size:1.1em;margin-left:35px;">5. 還款靈活，可申請提前還款;</p>
                </div>
            </div>
            <div class="row">
                <div style="padding: 0 80px;">
                    <h4>四、產品規格</h4>
                    <p style="font-size:1.1em;margin-left:35px;">1. 貸款期限：1個月～12個月、24個月;</p>
                    <p style="font-size:1.1em;margin-left:35px;">2. 月息最低0.5%;</p>
                </div>
            </div>
            <div class="row">
                <div style="padding: 0 80px;">
                    <h4>五、申請資料</h4>
                    <p style="font-size:1.1em;margin-left:35px;">1. 有效的中華民國國民身份證;</p>
                    <p style="font-size:1.1em;margin-left:35px;">2. 申請辦理抵押的不動產所有權狀;</p>
                    <p style="font-size:1.1em;margin-left:35px;">3. 與貸款申請相關的其他資料。</p>
                </div>
            </div>
            <div class="row">
                <div style="padding: 0 80px;">
                    <h4>六、申辦流程</h4>
                    <p style="font-size:1.1em;margin-left:35px;">1. 加入LINE ID@liq9224x;</p>                    
                    <p style="font-size:1.1em;margin-left:35px;">2. 將債權抵押文件掃描或拍照後傳送給Line ID：@liq9224x;</p>                    
                    <p style="font-size:1.1em;margin-left:35px;">3. 媒合天使投資人;</p>                    
                    <p style="font-size:1.1em;margin-left:35px;">4. 債權商城審查貸款資格合格後進行貸款簽約;</p>                    
                    <p style="font-size:1.1em;margin-left:35px;">5. 銀行撥放借貸金額至您指定的帳戶;</p>                    
                </div>
            </div>
        </div>
    `
})