const componentMyBanner2 = Vue.component('my-banner-2', {
    template: `
        <div class="container">
            <div class="gap"></div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-center">
                    <div class="row">
                        <h4 style="color:#000;">急用錢! 找債權商城，用Line申辦真便利!</h4>
                    </div>
                    <div class="row">
                        <ul class="banner2-list">
                            <li class="banner2-list-item">
                                <span>加入Line好友</span>
                                <span>ID:@liq9224x</span>
                                <a href="https://line.me/R/ti/p/%40liq9224x" target="_blank" title="債權商城官方帳號@liq9224x">
                                    <img src="/img/lmarket_qr.png">
                                </a>
                            </li>
                            <li class="banner2-list-item">
                                <span>提供債權抵押文件</span>
                                <img src="/img/banner2-2.png">
                            </li>
                            <li class="banner2-list-item">
                                <span>貸款審查與簽約</span>
                                <img src="/img/banner2-3.png">
                            </li>
                            <li class="banner2-list-item">
                                <span>貸款撥放</span>
                                <img src="/img/banner2-4.png">
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <a href="https://line.me/R/ti/p/%40liq9224x" target="_blank"
                        class="btn btn-large" style="width:25%;line-height:2em;color:white;background:rgb(86, 156, 45);border-radius:10px;">
                            前往貸款
                        </a>                    
                    </div>

                </div>
            </div>
            <div class="gap"></div>
        </div>
    `,
    props: [],
    data: function() {
        return {

        }
    },
    computed: {

    },
    watch: {

    },
    methods: {

    },
    created() {

    },
    mounted() {

    }

})


