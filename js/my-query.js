const componentMyQuery = Vue.component('my-query', {
    template: `
        <div>
            <my-query-input
                :isFetching="isFetching"
                @estimate="handleEstimate">
            </my-query-input>
            <my-query-error :error="error"></my-query-error>
            <my-query-result 
                :result="queryResult" 
                :data="queryData">
            </my-query-result>
            <my-query-history 
                :history="queryHistory"
                @history-click="handleHistoryClick"
                @clear-history="clearHistory">
            </my-query-history>
            <button type="button" ref="myModal" class="btn hidden" data-toggle="modal" data-target="#myModal">
                Open Modal
            </button>
        </div>
    `,
    props: [],
    data: function() {
        return {
            error: '',
            queryResult: null,
            queryData: null,
            queryHistory: [],
            isFetching: false       
        }
    },
    computed: {

    },
    watch: {
        queryHistory(newHistory) {
            let key = 'houseQueryHistory'
            if(newHistory.length === 0) {
                localStorage.setItem(key, '')
            } else {
                localStorage.setItem(key, newHistory.join('&'))
            }                    
        }        
    },
    methods: {
        handleEstimate(data) {
            this.error = ''
            this.isFetching = true
            this.queryData = data
            // axios.post('http://122.116.192.15:8000/estimate', data)
            // axios.post('https://house-price-api.herokuapp.com/estimate', data)
            axios.post('http://localhost:8000/estimate', data)
                .then(res => {
                    console.log(res.data)
                    this.isFetching = false
                    if (res.data === 'over') {
                        return alert('超過每日查詢上限')
                    }
                    if(res.data.estimatedPrice == 0) {
                        return alert('查無資料，請檢查地址是否正確?')
                    }
                    this.queryResult = res.data
                    this.queryHistory.push(JSON.stringify({
                        data: this.queryData,
                        result: this.queryResult
                    }))
                    myMap(res.data.location)
                    this.$refs.myModal.click()
                })
                .catch(err => {
                    console.log(err)
                    this.isFetching = false
                    this.error = '輸入資料有誤，請重新查詢'
                })
        },
        clearHistory() {
            this.queryHistory = []
        },
        handleHistoryClick(history) {
            myMap(history.result.location)
            this.queryResult = history.result
            this.queryData = history.data
            this.$refs.myModal.click()
        }        
    },
    created() {

    },
    mounted() {
        let key = 'houseQueryHistory'
        let history = localStorage.getItem(key) || ''
        if(history !== '') {                   
            this.queryHistory = history.split('&')
            console.log('history length = ', this.queryHistory.length)
        }
    }

})

