const componentMyHeader = Vue.component('my-header', {
    template: `
        <div>
            <div class="col-xs-12" style="padding:0;">
                <div class="ken_logo">
                    <div class="ken_logo_div">
                        <a href="#">
                            <img src="/img/logo.png">
                        </a>
                    </div>
                    <div class="nav_top"
                         data-toggle="collapse" data-target="#myTopNav">
                        <span class="fa fa-bars fa-3x"></span>
                    </div>                    
                </div>
                <div class="collapse" id="myTopNav">
                    <ul class="nav navbar-nav nav_top_list">
                        <li>
                            <a href="https://lmarket.com.tw/index.action">
                                <span class="fa fa-home"></span>首頁
                            </a>
                        </li>
                        <li>
                            <a href="https://lmarket.com.tw/doCheckMemberLoginControll.action">
                                <span class="fa fa-user"></span>會員資料
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="fa fa-legal"></span>我要投資
                                <span class="fa fa-chevron-down pull-right"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="https://lmarket.com.tw/invetControll.action">投資策略</a></li>
                                <li><a href="https://lmarket.com.tw/doQueryMssControll.action">投資選擇</a></li>
                            </ul>
                        </li> 
                        <li>
                            <a href="">
                                <span class="fa fa-bank"></span>我要借貸
                            </a>
                        </li> 
                        <li>
                            <a href="https://lmarket.com.tw/showMyOrderControll.action">
                                <span class="fa fa-history"></span>投資紀錄
                            </a>
                        </li> 
                        <li>
                            <a href="https://lmarket.com.tw/qaStepOneControll.action">
                                <span class="fa fa-info"></span>問與答
                            </a>
                        </li> 
                        <li>
                            <a href="https://line.me/R/ti/p/%40liq9224x">
                                <span class="line_top_nav"></span>LINE ID:@liq9224x
                            </a>
                        </li> 
                    </ul>
                </div>                
            </div>
            <div class="gap"></div>
            <div class="gap"></div>
        </div>
    `,
    props: [],
    data: function() {
        return {

        }
    },
    computed: {

    },
    watch: {

    },
    methods: {

    },
    created() {

    },
    mounted() {

    }

})


