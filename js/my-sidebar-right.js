const componentMySidebarRight = Vue.component('my-sidebar-right', {
    template: `
        <div class="nav_right">
            <ul>
                <li style="background:#000;">
                    <span style="display:inline-block;margin-top:15px;position:relative;left:-20px;">
                        <span style="font-size:1.2em;font-weight:bold;">免費</span>借貸諮詢
                    </span>
                </li>
                <li>
                    <a href="https://line.me/R/ti/p/%40liq9224x" title="加入Line好友">
                        <span class="nav_right_box nav_right_box_1"></span>
                    </a>
                </li>
                <li>
                    <a href="mailto:lmarket@lmarket.com.tw" title="歡迎寫信與我們聯絡">
                        <span class="nav_right_box nav_right_box_2"></span>
                    </a>
                </li>
                <li>
                    <a href="tel:+886-2-27060066" title="打電話與我們聯絡">
                        <span class="nav_right_box nav_right_box_3"></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.google.com.tw/maps/place/106%E5%8F%B0%E5%8C%97%E5%B8%82%E5%A4%A7%E5%AE%89%E5%8D%80%E5%BE%A9%E8%88%88%E5%8D%97%E8%B7%AF%E4%B8%80%E6%AE%B5243%E8%99%9F/@25.0372684,121.5417403,17z/data=!3m1!4b1!4m5!3m4!1s0x3442abd143190933:0x57385e3dc33fd852!8m2!3d25.0372636!4d121.5439344" 
                    target="_blank"
                    title="公司地圖">
                        <span class="nav_right_box nav_right_box_4"></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/Lmarket.taiwan/?fref=ts" 
                    target="_blank"
                    title="Facebook粉絲團">
                        <span class="nav_right_box nav_right_box_5"></span>
                    </a>
                </li>                
            </ul>
        </div>   
    `,
    props: [],
    data: function() {
        return {

        }
    },
    computed: {

    },
    watch: {

    },
    methods: {

    },
    created() {

    },
    mounted() {

    }

})
