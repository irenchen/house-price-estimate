Vue.filter('myPriceFilter', function(value) {
    if(!value) return ''
    value = value.toString()
    let digits = value.split('')
    digits.reverse()
    if(digits.length > 3) {
        return [...digits.slice(0,3), ',', ...digits.slice(3)].reverse().join('')      
    }    
    return value
})