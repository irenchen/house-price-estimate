const componentMyBanner2Tst = Vue.component('my-banner-2-tst', {
    template: `
        <div class="container">
            <div class="gap"></div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-center">
                    <div class="row">
                        <h4 style="color:#000;">急用錢! 找債權商城，用Line申辦真便利!</h4>
                    </div>
                    <div class="row" style="padding-bottom:10px;">
                        <a href="https://line.me/R/ti/p/%40liq9224x" target="_blank">                            
                            <img :src="imgSrc" alt="前往貸款"
                                width="100%" />
                        </a>
                    </div>
                    <div class="row hidden">
                        <a href="https://line.me/R/ti/p/%40liq9224x" target="_blank"
                        class="btn btn-large" style="width:25%;line-height:2em;color:white;background:rgb(86, 156, 45);border-radius:10px;">
                            前往貸款
                        </a>                    
                    </div>

                </div>
            </div>
            <div class="gap"></div>
        </div>
    `,
    props: [],
    data: function() {
        return {

        }
    },
    computed: {
        imgSrc () {
            return window.innerWidth <= 500 ? '/img/banner2-small.gif' : '/img/banner2.gif'
        }
    },
    watch: {

    },
    methods: {

    },
    created() {
        
    },
    mounted() {
        
    }

})


