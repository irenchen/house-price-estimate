const componentMySidebarLeft = Vue.component('my-sidebar-left', {
    template: `
        <div class="nav_ken">
            <ul>
                <a href="https://lmarket.com.tw/index.action"><li >首頁</li></a>
                <a href="https://lmarket.com.tw/doCheckMemberLoginControll.action"><li>會員資料</li></a>
                <div class="nav_ken_wish">
                    <div class="nav_ken_wish_One">我要投資</div>
                    <div class="nav_ken_wish_Two">
                        <a href="https://lmarket.com.tw/invetControll.action">
                            <i class="fa fa-lightbulb-o"></i>投資策略
                        </a>
                    </div>
                    <div class="nav_ken_wish_Two">
                        <a href="https://lmarket.com.tw/doQueryMssControll.action">
                            <i class="fa fa-legal"></i>投資選擇
                        </a>
                    </div>
                </div>
                <a href=""><li style="background-color:#8dbc1e;margin:0;">我要借貸</li></a>
                <a href="https://lmarket.com.tw/showMyOrderControll.action"><li>投資紀錄</li></a>
                <a href="https://lmarket.com.tw/qaStepOneControll.action"><li>問與答</li></a>
                
            </ul>
            <a href="https://line.me/R/ti/p/%40liq9224x">
            <div class="line_ken"></div>
            </a>            
        </div>
    `,
    props: [],
    data: function() {
        return {

        }
    },
    computed: {

    },
    watch: {

    },
    methods: {

    },
    created() {

    },
    mounted() {

    }

})


