const componentMyBanner1 = Vue.component('my-banner-1', {
    template: `
        <div class="row" style="margin: 0;">
            <div class="col-sm-10 col-sm-offset-1">
                <div id="slick">
                    <h1 class="banner1-h1">
                        <a href="loan_demo.html" target="_blank" title="了解更多">
                            <div style="height:100%"></div>
                        </a>
                    </h1>
                    <h1 class="banner1-h2">
                        <a href="loan_demo.html" target="_blank" title="了解更多">
                            <div style="height:100%"></div>
                        </a>
                    </h1>
                    <h1 class="banner1-h3">
                        <a href="loan_demo.html" target="_blank" title="了解更多">
                            <div style="height:100%"></div>
                        </a>
                    </h1>
                </div>
            </div>
        </div>
    `,
    props: [],
    data: function() {
        return {
            screenWidth: window.innerWidth,
        }
    },
    computed: {
        // bannerStyle() {
        //     return {
        //         height: Math.floor(this.screenWidth / 4) + 'px'
        //     }
        // },
        // bannerHeight() {
        //     return Math.floor(this.screenWidth / 4) + 'px'
        // }
    },
    watch: {

    },
    methods: {
        handleResize(evt) {
            // console.log(`window resize to ${window.innerWidth}`)
            this.screenWidth = window.innerWidth
        }
    },
    created() {
        window.addEventListener('resize', this.handleResize)
    },
    mounted() {
        $(function() {
            $('#slick')
                .slick({
                    autoplay: true,
                    autoplaySpeed: 2000,
                    pauseOnFocus: true,
                    pauseOnHover: true,
                    dots: true,
                    arrows: false,
                })
        })

    }

})


