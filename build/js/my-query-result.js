Vue.component('my-query-result', {
    template: `
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title">查詢結果</h4>
                    </div>
                    <div class="modal-body">
                        <div class="well" style="background:#569C2D;color:#eee;">
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <h3>總價 : {{ housePrice | myPriceFilter }}萬元
                                        <span style="font-size:0.5em;position:relative;left:10px;">
                                            {{ houseAverage }}萬 / 坪
                                        </span>
                                    </h3>
                                    <h3>貸給您 : {{ possibleLoan | myPriceFilter }}萬元</h3>
                                </div>
                                <div class="col-xs-12 col-sm-5 text-center">
                                    <a href="https://line.me/R/ti/p/%40liq9224x" target="_blank">
                                        <img src="/img/lmarket_qr.png" style="width:100px;height:100px;">
                                        <span style="display:inline-block;margin-left:10px;color:#000;">
                                            加入好友<span style="color:#eee;">@liq9224x</span>立即諮詢
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div style="display:flex; flex-wrap:wrap; justify-content: center; align-items: center;">
                            <div style="width:40%; lineheight:3em;">地區 : {{ city + district }}</div>
                            <div style="width:40%; lineheight:3em;">地址 : {{ houseAddress }}</div>
                            <div style="width:40%; lineheight:3em;">類型 : {{ houseType }}</div>
                            <div style="width:40%; lineheight:3em;">屋齡 : {{ houseAge }}</div>
                            <div style="width:40%; lineheight:3em;">樓層 : {{ houseFloor }}/{{ houseTotalFloor }}</div>
                            <div style="width:40%; lineheight:3em;">建坪 : {{ houseArea }}</div>
                        </div>
                        <div id="googleMap" style="width:80%;height:300px;margin: auto;"></div>
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button"
                            style="background:#569C2D;color:#eee;width:40%;"
                            class="btn btn-default" data-dismiss="modal">
                            重新查詢
                        </button>
                    </div>
                    
                </div>
            </div>
        </div>            
    `,
    props: ['data', 'result'],
    data: function() {
        return {
            cities: [
                '台北市', '新北市', '桃園市', '台中市', '台南市', '高雄市',
                '新竹市', '新竹縣', '基隆市', '嘉義市', '嘉義縣', '宜蘭縣',
                '花蓮縣', '苗栗縣', '彰化縣', '南投縣', '雲林縣', '屏東縣',
                '台東縣'
            ],            
            districts: [
                ['中正區', '大同區', '中山區', '松山區', '大安區', '萬華區', '信義區', '士林區', '北投區', '內湖區', '南港區', '文山區'],
                ['萬里區', '金山區', '板橋區', '汐止區', '深坑區', '石碇區', '瑞芳區', '平溪區', '雙溪區', '貢寮區', '新店區', '坪林區', '烏來區', '永和區', '中和區', '土城區', '三峽區', '樹林區', '鶯歌區', '三重區', '新莊區', '泰山區', '林口區', '蘆洲區', '五股區', '八里區', '淡水區', '三芝區', '石門區'],
                [],
                ['中區', '東區', '南區', '西區', '北區', '北屯區', '西屯區', '南屯區', '太平區', '大里區', '霧峰區', '烏日區', '豐原區', '后里區', '石岡區', '東勢區', '和平區', '新社區', '潭子區', '大雅區', '神岡區', '大肚區', '沙鹿區', '龍井區', '梧棲區', '清水區', '大甲區', '外埔區', '大安區'],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                [],
                []
            ],
            types: [
                '套房', '公寓(5層含以下無電梯)', '華廈(10層含以下有電梯)',
                '住宅大樓(11層含以上有電梯)', '透天'
            ],
        }
    },
    computed: {
        city () {
            return this.data && this.cities[this.data.city - 1]
        },
        district () {
            return this.data && this.districts[this.data.city - 1][this.data.district - 1]
        },
        houseAddress () {
            return this.data && this.data.address
        },
        houseType () {
            return this.data && this.types[this.data.houseType - 1]
        },
        houseAge () {
            return this.data && this.data.age
        },
        houseArea () {
            return this.data && this.data.area
        },
        houseFloor () {
            return this.data && this.data.floor
        },
        houseTotalFloor () {
            return this.data && this.data.totalFloor
        },
        housePrice () {
            return this.result && this.result.estimatedPrice
        },
        houseAverage () {
            return this.result && this.result.avg
        },
        possibleLoan () {
            return this.result && this.result.possibleLoan
        }
    },
    watch: {

    },
    methods: {

    },
    created() {

    },
    mounted() {

    }

})

