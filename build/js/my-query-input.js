const componentMyQueryInput = Vue.component('my-query-input', {
    template: `
        <div class="container">
            <div class="row" style="width:80%;margin: 0 auto;">
                <p style="font-size:1.1em;">試算行情</p>
                <p style="border:0.5px solid #999;"></p>
            </div>
            <div class="row">
                <div class="col-sm-2 col-xs-2 text-center">
                    <label style="position:absolute;right:10px;">
                        <i style="color:red;">*</i>地址:
                    </label>
                </div>
                <div class="col-sm-2 col-xs-5 form-group">
                    <select name="city" id="city" class="form-control"
                            v-model="cityIndex"
                            :class="{ 'form-alert': alertCity }">
                        <option value="0" selected>選擇城市</option>
                        <option :value="index + 1" v-for="(city, index) in cities">
                                {{ city }}
                        </option>
                    </select>
                </div>
                <div class="col-sm-2 col-xs-5 form-group">
                    <select name="district" id="district" class="form-control"
                            v-model="districtIndex"
                            :class="{ 'form-alert': alertDistrict }">
                        <option value="0" selected>選擇地區</option>
                        <option :value="index + 1" v-for="(dis, index) in districts">
                            {{ dis }}
                        </option>
                    </select>
                </div>
                <div class="col-sm-6 col-xs-10 form-group xs-address">
                    <input type="text" name="address" id="address" 
                        class="form-control" placeholder="請輸入路段及屋號，如復興南路一段243號"
                        v-model="address"
                        :class="{ 'form-alert': alertAddress }"
                        style="width:80%;display:inline-block;">
                    <!-- <button>地圖找點</button> -->
                </div>
            </div>
            <div style="height:20px;"></div>
            <div class="row">
                <div class="col-xs-2 text-center">
                    <label style="position:absolute;right:10px;">
                        <i style="color:red;">*</i>類型:
                    </label>
                </div>
                <div class="col-xs-10 form-group"
                    :class="{ 'form-alert': alertHouseType }">
                    <label class="radio-inline" v-for="(type, index) in types">
                        <input type="radio" name="houseType"
                            :value="index + 1" v-model="houseTypeIndex">
                            {{ type }}
                    </label>
                </div>
            </div>
            <div style="height:20px;"></div>
            <div class="row">
                <div class="col-xs-2 text-center">
                    <label style="position:absolute;right:10px;">
                        <i style="color:red;">*</i>樓層:
                    </label>
                </div>
                <div class="col-xs-4 form-group">
                    <select v-model="selectedFloor" class="form-control"
                            @click="verifyFloorClick"
                            :class="{ 'form-alert': alertFloor }">
                        <option value="0" selected>所在樓層</option>
                        <option v-for="(opt, index) in floorOpts">{{ opt }}</option>
                    </select>
                    <p style="color:#569C2D;">不包含頂樓加蓋</p>
                </div>              
                <div class="col-xs-1">/</div>
                <div class="col-xs-4">
                    <select v-model="selectedTotalFloor" class="form-control"                    
                            @click="verifyFloorClick"
                            :class="{ 'form-alert': alertTotalFloor }">
                        <option value="0" selected>總樓層</option>
                        <option v-for="(opt, index) in totalFloorOpts">{{ opt }}</option>
                    </select>
                    <p style="color:#569C2D;">不包含一樓物件</p>
                </div>
                <div class="col-xs-2 checkbox" v-show="isTop && houseTypeIndex == 2">
                    <label for="topOver">
                        <input type="checkbox" name="hasTopOver" v-model="hasTopOver">有頂加
                    </label>
                </div>
            </div>
            <div style="height:20px;"></div>
            <div class="row">
                <div class="col-xs-2 text-center">
                    <label style="position:absolute;right:10px;">
                        <i style="color:red;">*</i>屋齡:
                    </label>
                </div>
                <div class="col-sm-4 col-xs-6 form-group">
                    <input type="number" class="form-control"
                        :class="{ 'form-alert': alertAge }"
                        min="0" max="80" v-model="age" @change="verifyAge"
                        name="age" id="age">
                </div>
                <div class="col-sm-2 col-xs-3 checkbox hidden">
                    <label for="managed">
                        <input type="checkbox" name="hasManage"                               
                               v-model="hasManage">
                            有管理
                    </label>
                </div>
            </div>
            <div style="height:20px;"></div>
            <div class="row">
                <div class="col-xs-2 text-center">
                    <label style="position:absolute;right:10px;"><i style="color:red;">*</i>建坪:</label>
                </div>
                <div class="col-xs-6 col-sm-4 form-group">
                    <input type="text" class="form-control"
                        :class="{ 'form-alert': alertHouseArea }"
                        name="houseArea" id="houseArea"
                        v-model="houseArea" 
                        placeholder="請輸入登記建坪">
                    <p style="color:#569C2D;">含公設不含車位</p>
                </div>
            </div>
            <div class="row text-center" v-show="!!errorMessage">
                <div class="alert alert-danger">{{ errorMessage }}</div>
            </div>
            <div class="row text-center" style="color:#569C2D;">
                溫馨提醒：本試算結果僅供參考，實際金額由債權商城專業估價師估價結果為準
            </div>
            <div style="height:20px;"></div>
            <div class="row text-center">
                <div id="submit" class="col-md-12">
                    <button class="btn btn-lg btn-default"
                            style="background:#569C2D;color:#eee;width:40%;"
                            @click="handleSubmit"
                            :disabled="false">
                        確認送出
                    </button>
                </div>
            </div>
        </div>
    `,
    props: [],
    data: function() {
        return {
            errorMessage: '',
            cities: cities,
            cityIndex: 0,
            districts: [],
            districtIndex: 0,
            types: [
                '套房', '公寓(5層含以下無電梯)', '華廈(10層含以下有電梯)',
                '住宅大樓(11層含以上有電梯)', '透天'
            ],
            houseTypeIndex: 0,
            address: '',
            age: null,
            hasManage: false,
            floor: null,
            totalFloor: null,
            floorOpts: [],
            selectedFloor: 0,
            totalFloorOpts: [],
            selectedTotalFloor: 0,
            isTop: false,
            hasTopOver: false,
            houseArea: null,
            formSubmitCount: 0,
        }
    },
    computed: {
        disableBtn () {
            if (this.cityIndex !== 0 && this.districtIndex !== 0 && this.houseTypeIndex == 5 &&
                this.address.trim() !== '' && this.selectedFloor == 0 && this.selectedTotalFloor == 0 &&
                this.houseArea && this.age && this.errorMessage == '') {
                return false
            }
            return this.cityIndex == 0 || this.districtIndex == 0 || this.houseTypeIndex == 0 ||
                this.address.trim() == '' || this.selectedFloor == 0 || this.selectedTotalFloor == 0 ||
                !this.houseArea || !this.age || this.errorMessage != ''
        },
        alertCity () {
            return this.formSubmitCount > 0 && this.cityIndex == 0
        },
        alertDistrict () {
            return this.formSubmitCount > 0 && this.districtIndex == 0
        },
        alertHouseType () {
            return this.formSubmitCount > 0 && this.houseTypeIndex == 0
        },
        alertAddress () {
            return this.formSubmitCount > 0 && this.address.trim() == ''
        },
        alertFloor () {
            return this.formSubmitCount > 0 && this.selectedFloor == 0 && this.houseTypeIndex !== 5
        },
        alertTotalFloor () {
            return this.formSubmitCount > 0 && this.selectedTotalFloor == 0 && this.houseTypeIndex !== 5
        },
        alertAge () {
            return this.formSubmitCount > 0 && !this.age
        },
        alertHouseArea () {
            return this.formSubmitCount > 0 && !this.houseArea
        }
    },
    watch: {
        address: function(newAddress) {
            if(/^[0-9a-zA-Z]/.test(newAddress)) {
                this.errorMessage = '地址格式錯誤'
            } else {
                this.errorMessage = ''
            }
        },
        cityIndex: function(newIndex) {
            // console.log("new cityIndex :", newIndex)
            this.districts = districts[newIndex-1]
        },
        districtIndex: function(newIndex) {
            console.log("new districtIndex : ", newIndex)
        },
        houseTypeIndex: function(newIndex) {
            // console.log("new houseTypeIndex : ", newIndex)
            this.errorMessage = ''
            this.totalFloorOpts = this.updateTotalFloorOpts(newIndex)
            this.floorOpts = this.updateFloorOpts(newIndex)
        },
        selectedTotalFloor: function(newIndex) {
            // console.log("selected floor : ", this.selectedFloor)
            // console.log("selected TotalFloor : ", this.selectedTotalFloor)
            if(this.selectedFloor == this.selectedTotalFloor) {
                this.isTop = true
            } else {
                this.isTop = false
            }
        },
        selectedFloor: function(newIndex) {
            // console.log("selected floor : ", this.selectedFloor)
            // console.log("selected TotalFloor : ", this.selectedTotalFloor)
            if(this.selectedFloor == this.selectedTotalFloor) {
                this.isTop = true
            } else {
                this.isTop = false
            }
        },
        hasTopOver: function(newValue) {
            console.log('hasTopOver : ', newValue)
        },
        houseArea: function(newValue) {
            if(newValue === '') {
                return this.errorMessage = ''
            } else if(parseFloat(newValue) <= 0) {
                return this.errorMessage = '建坪不得少於0'
            } else if(isNaN(parseFloat(newValue))) {
                return this.errorMessage = '建坪欄位請輸入數字'
            } else {
                this.errorMessage = ''
            }
        }
    },
    methods: {
        verifyAge(evt) {
            if(evt.target.value > 80) {
                this.errorMessage = '屋齡不得超過80年'
            } else if(evt.target.value < 0) {
                this.errorMessage = '屋齡不得少於0年'
            } else {
                this.errorMessage = ''
            }
        },
        verifyFloorClick() {
            if(this.houseTypeIndex == 0) {
                this.errorMessage = '請先選擇房屋類型'
            } else if(this.selectedFloor == 0) {
                // this.errorMessage = '請選擇所在樓層'
            } else if(this.selectedTotalFloor == 0) {
                // this.errorMessage = '請選擇總樓層'
            } else {
                this.errorMessage = ''
            }
        },
        updateTotalFloorOpts(index) {
            switch(index) {
                case 1: // 套房
                    return _.range(2, 41)
                case 2: // 公寓(5層含以下無電梯)
                    return _.range(2, 6)
                case 3: // 華廈(10層含以下有電梯)
                    return _.range(2, 11)
                case 4: // 住宅大樓(11層含以上有電梯)
                    return _.range(11, 41)
                case 5: // 透天
                    return []
                default:
                    console.log('incorrect index')
                    return []
            }
        },
        updateFloorOpts(index) {
            switch(index) {
                case 1: // 套房
                    return _.range(1, 41)
                case 2: // 公寓(5層含以下無電梯)
                    return _.range(1, 6)
                case 3: // 華廈(10層含以下有電梯)
                    return _.range(1, 11)
                case 4: // 住宅大樓(11層含以上有電梯)
                    return _.range(1, 41)
                case 5: // 透天
                    return []
                default:
                    console.log('incorrect index')
                    return []
            }
        },
        verifyFormData(data) {
            
        },
        handleSubmit(evt) {
            evt.preventDefault()
            this.formSubmitCount++
            if(this.disableBtn) return
            console.log('submitted...')
            formData = {
                city: this.cityIndex,
                district: this.districtIndex,
                address: this.address,
                houseType: this.houseTypeIndex,
                floor: this.selectedFloor,
                totalFloor: this.selectedTotalFloor,
                age: this.age,
                area: parseFloat(this.houseArea),
                hasTopOver: this.hasTopOver,
                hasManage: this.hasManage,

            }
            this.$emit('estimate', formData)
        }
    },
    created() {

    },
    mounted() {

    }

})

const cities = [
    '台北市', '新北市', '桃園市', '台中市', '台南市', '高雄市',
    '新竹市', '新竹縣', '基隆市', '嘉義市', '嘉義縣', '宜蘭縣',
    '花蓮縣', '苗栗縣', '彰化縣', '南投縣', '雲林縣', '屏東縣',
    '台東縣'
]

const districts = [
    ['中正區', '大同區', '中山區', '松山區', '大安區', '萬華區', '信義區', '士林區', '北投區', '內湖區', '南港區', '文山區'],
    ['萬里區', '金山區', '板橋區', '汐止區', '深坑區', '石碇區', '瑞芳區', '平溪區', '雙溪區', '貢寮區', '新店區', '坪林區', '烏來區', '永和區', '中和區', '土城區', '三峽區', '樹林區', '鶯歌區', '三重區', '新莊區', '泰山區', '林口區', '蘆洲區', '五股區', '八里區', '淡水區', '三芝區', '石門區'],
    [],
    ['中區', '東區', '南區', '西區', '北區', '北屯區', '西屯區', '南屯區', '太平區', '大里區', '霧峰區', '烏日區', '豐原區', '后里區', '石岡區', '東勢區', '和平區', '新社區', '潭子區', '大雅區', '神岡區', '大肚區', '沙鹿區', '龍井區', '梧棲區', '清水區', '大甲區', '外埔區', '大安區'],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    []
]
