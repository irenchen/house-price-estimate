const componentMyQueryTest = Vue.component('my-query-test', {
    template: `
        <div>
            <my-query-input
                @estimate="handleEstimate">
            </my-query-input>
            <my-query-error :error="error"></my-query-error>
            <my-query-result-test
                :result="queryResult" 
                :data="queryData"
                :nearbyHouses="nearbyHouses">
            </my-query-result-test>
            <button type="button" ref="myModal" class="btn hidden" data-toggle="modal" data-target="#myModal">
                Open Modal
            </button>
        </div>
    `,
    props: [],
    data: function() {
        return {
            error: '',
            queryResult: null,
            queryData: null,
            queryHistory: [],
            nearbyHouses: [],            
        }
    },
    computed: {

    },
    watch: {
        
    },
    methods: {
        handleEstimate(data) {
            this.error = ''
            this.queryData = data
            // axios.post('http://122.116.192.15:8000/estimate', data)
            axios.post('http://localhost:8000/nearbyHouses', data)
                .then(res => {
                    console.log(res.data.nearbyHouses.length)
                    this.queryResult = res.data
                    console.log(res.data.nearbyHouses[0])
                    this.nearbyHouses = res.data.nearbyHouses
                    myMapAll(res.data.location, res.data.nearbyHouses)
                    this.$refs.myModal.click()
                })
                .catch(err => {
                    console.log(err)
                    this.error = '輸入資料有誤，請重新查詢'
                })
        },      
    },
    created() {

    },
    mounted() {

    }

})

