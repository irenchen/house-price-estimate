const componentMyQueryError = Vue.component('my-query-error', {
    template: `
        <div class="container" style="width:80%;margin-top:10px;">
            <div class="row text-center" v-show="!!error">
                <div class="alert alert-danger">{{ error }}</div>
            </div>
        </div>
    `,
    props: ['error']
})