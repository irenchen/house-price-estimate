const componentMyQuery = Vue.component('my-query', {
    template: `
        <div class="container" style="width: 80%;">
            <v-container>
                <v-layout row wrap>
                    <v-flex xs12>
                        
                    </v-flex>
                </v-layout>
            
                <div class="row">
                    <div class="col-sm-2 col-xs-2 text-center">
                        <label>*地址:</label>
                    </div>
                    <div class="col-sm-2 col-xs-4 form-group">
                        <select name="city" id="city" class="form-control" 
                                v-model="cityIndex">
                            <option value="0" selected>選擇城市</option>
                            <option :value="index + 1" v-for="(city, index) in cities">
                                    {{ city }}
                            </option>
                        </select>
                    </div>
                    <div class="col-sm-2 col-xs-4 form-group">
                        <select name="district" id="district" class="form-control"
                                v-model="districtIndex">
                            <option value="0" selected>選擇地區</option>
                            <option :value="index + 1" v-for="(dis, index) in districts">
                                {{ dis }}
                            </option>
                        </select>
                    </div>
                    <div class="col-sm-6 col-xs-10 form-group">
                        <input type="text" name="address" id="address" 
                            class="form-control" placeholder="請輸入路段及屋號，如復興南路一段243號"
                            v-model="address">
                    </div>
                </div>
            <div style="height:20px;"></div>
            <div class="row">
                <div class="col-xs-2 text-center">
                    <label>*類型:</label>
                </div>
                <div class="col-xs-10">
                    <label class="radio-inline" v-for="(type, index) in types">
                        <input type="radio" name="houseType"
                            :value="index + 1" v-model="houseTypeIndex">
                            {{ type }}
                    </label>
                </div>
            </div>
            <div style="height:20px;"></div>
            <div class="row">
                <div class="col-xs-2 text-center">
                    <label>*樓層:</label>
                </div>
                <div class="col-xs-3 form-group">
                    <select v-model="selectedFloor" class="form-control"
                            @click="verifyFloorClick">
                        <option value="0" selected>所在樓層</option>
                        <option v-for="(opt, index) in floorOpts">{{ opt }}</option>
                    </select>
                    <p style="color:#569C2D;">不包含頂樓加蓋</p>
                </div>              
                <div class="col-xs-1">/</div>
                <div class="col-xs-3">
                    <select v-model="selectedTotalFloor" class="form-control"
                            @click="verifyFloorClick">
                        <option value="0" selected>總樓層</option>
                        <option v-for="(opt, index) in totalFloorOpts">{{ opt }}</option>
                    </select>
                    <p style="color:#569C2D;">不包含一樓物件</p>
                </div>
                <div class="col-xs-2 checkbox" v-show="isTop && houseTypeIndex == 2">
                    <label for="topOver">
                        <input type="checkbox" name="hasTopOver" v-model="hasTopOver">有頂加
                    </label>
                </div>
            </div>
            <div style="height:20px;"></div>
            <div class="row">
                <div class="col-xs-2 text-center">
                    <label>*屋齡:</label>
                </div>
                <div class="col-sm-2 col-xs-3 form-group">
                    <input type="number" class="form-control"
                           min="0" max="80" v-model="age" @change="verifyAge"
                           name="age" id="age">
                </div>
                <div class="col-sm-2 col-xs-3 checkbox">
                    <label for="managed">
                        <input type="checkbox" name="hasManage"
                               v-model="hasManage">
                            有管理
                    </label>
                </div>
            </div>
            <div style="height:20px;"></div>
            <div class="row">
                <div class="col-xs-2 text-center">
                    <label>*建坪:</label>
                </div>
                <div class="col-xs-6 col-sm-4 form-group">
                    <input type="text" class="form-control" 
                            name="houseArea" id="houseArea"
                            v-model="houseArea" 
                            placeholder="請輸入登記建坪">
                    <p style="color:#569C2D;">含公設不含車位</p>
                </div>
            </div>
            <div class="row text-center" v-show="!!errorMessage">
                <div class="alert alert-danger">{{ errorMessage }}</div>
            </div>
                <div class="row text-center" style="color:#569C2D;">
                    溫馨提醒：本試算結果僅供參考，實際金額仍以各銀行或相關貸款單位為準
                </div>
                <div style="height:20px;"></div>
                <v-layout row wrap text-xs-center>
                    <v-flex xs12>
                        <v-btn large
                            style="background:#569C2D;color:#eee;width:40%;"
                            @click="handleSubmit">
                            確認送出
                        </v-btn>
                    </v-flex>
                </v-layout>
            </v-container>
        </div>
    `,
    props: [],
    data: function() {
        return {
            errorMessage: '',
            cities: cities,
            cityIndex: 0,
            districts: [],
            districtIndex: 0,
            types: [
                '套房', '公寓(5層含以下無電梯)', '華廈(10層含以下有電梯)',
                '住宅大樓(11層含以上有電梯)', '透天'
            ],
            houseTypeIndex: 0,
            address: '',
            age: null,
            hasManage: false,
            floor: null,
            totalFloor: null,
            floorOpts: [],
            selectedFloor: 0,
            totalFloorOpts: [],
            selectedTotalFloor: 0,
            isTop: false,
            hasTopOver: false,
            houseArea: null,
        }
    },
    computed: {

    },
    watch: {
        cityIndex: function(newIndex) {
            console.log("new cityIndex :", newIndex)
            this.districts = districts[newIndex-1]
        },
        districtIndex: function(newIndex) {
            console.log("new districtIndex : ", newIndex)
        },
        houseTypeIndex: function(newIndex) {
            console.log("new houseTypeIndex : ", newIndex)
            this.errorMessage = ''
            this.totalFloorOpts = this.updateTotalFloorOpts(newIndex)
            this.floorOpts = this.updateFloorOpts(newIndex)
        },
        selectedTotalFloor: function(newIndex) {
            console.log("selected floor : ", this.selectedFloor)
            console.log("selected TotalFloor : ", this.selectedTotalFloor)
            if(this.selectedFloor == this.selectedTotalFloor) {
                this.isTop = true
            } else {
                this.isTop = false
            }
        },
        selectedFloor: function(newIndex) {
            console.log("selected floor : ", this.selectedFloor)
            console.log("selected TotalFloor : ", this.selectedTotalFloor)
            if(this.selectedFloor == this.selectedTotalFloor) {
                this.isTop = true
            } else {
                this.isTop = false
            }
        },
        hasTopOver: function(newValue) {
            console.log('hasTopOver : ', newValue)
        },
        houseArea: function(newValue) {
            if(parseFloat(newValue) <= 0) {
                this.errorMessage = '建坪不得少於0'
            } else {
                this.errorMessage = ''
            }
        }
    },
    methods: {
        verifyAge($evt) {
            if($evt.target.value > 80) {
                this.errorMessage = '屋齡不得超過80年'
            } else if($evt.target.value < 0) {
                this.errorMessage = '屋齡不得少於0年'
            } else {
                this.errorMessage = ''
            }
        },
        verifyFloorClick() {
            if(this.houseTypeIndex == 0) {
                this.errorMessage = '請先選擇房屋類型'
            } else {
                this.errorMessage = ''
            }
        },
        updateTotalFloorOpts(index) {
            switch(index) {
                case 1: // 套房
                    return _.range(2, 41)
                case 2: // 公寓(5層含以下無電梯)
                    return _.range(2, 6)
                case 3: // 華廈(10層含以下有電梯)
                    return _.range(2, 11)
                case 4: // 住宅大樓(11層含以上有電梯)
                    return _.range(11, 41)
                case 5: // 透天
                    return []
                default:
                    console.log('incorrect index')
                    return []
            }
        },
        updateFloorOpts(index) {
            switch(index) {
                case 1: // 套房
                    return _.range(1, 41)
                case 2: // 公寓(5層含以下無電梯)
                    return _.range(1, 6)
                case 3: // 華廈(10層含以下有電梯)
                    return _.range(1, 11)
                case 4: // 住宅大樓(11層含以上有電梯)
                    return _.range(1, 41)
                case 5: // 透天
                    return []
                default:
                    console.log('incorrect index')
                    return []            
            }
        },
        verifyFormData(data) {
            
        },
        handleSubmit($evt) {
            $evt.preventDefault()
            console.log('submitted...')
            formData = {
                city: this.cityIndex,
                district: this.districtIndex,
                address: this.address,
                houseType: this.houseTypeIndex,
                floor: this.selectedFloor,
                totalFloor: this.selectedTotalFloor,
                age: this.age,
                area: this.houseArea,
                hasTopOver: this.hasTopOver,
                hasManage: this.hasManage,

            }
            this.$emit('estimate', formData)
        }
    },
    created() {

    },
    mounted() {

    }

})

const cities = [
    '台北市',
    '新北市',
    '桃園市',
    '台中市',
    '台南市',
    '高雄市',
    '新竹市',
    '新竹縣',
    '基隆市',
    '嘉義市',
    '嘉義縣',
    '宜蘭縣',
    '花蓮縣',
    '苗栗縣',
    '彰化縣',
    '南投縣',
    '雲林縣',
    '屏東縣',
    '台東縣',
]

const districts = [
    ['中正區', '大同區', '中山區', '松山區', '大安區', '萬華區', '信義區', '士林區', '北投區', '內湖區', '南港區', '文山區'],
    ['a', 'b', 'c'],
    ['d', 'e', 'f'],
]
