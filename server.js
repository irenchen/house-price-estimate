const restify = require('restify')
const corsMiddleware = require('restify-cors-middleware')
const UTILS = require('./utils')
const CITIES = require('./utils/cities')
const DISTRICTS = require('./utils/districts')
const PLATEAU = 3.3058 // 1 plateau = 3.3058 m2
const fs = require('fs')

// Setup some https server options
//generated from http://www.selfsignedcertificate.com/
// const https_options = {
//     name: 'myapp',
//     version: '0.0.1',
//     key: fs.readFileSync('./HTTPS.key'),
//     certificate: fs.readFileSync('./HTTPS.cert')
// }
// const server = restify.createServer(https_options)

const server = restify.createServer({
    name: 'myapp',
    version: '0.0.1'
})

const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional 
    origins: ['*'],
    allowHeaders: ['API-Token'],
    exposeHeaders: ['API-Token-Expiry']
})
   
server.pre(cors.preflight)
server.use(cors.actual)

server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser())

// check visitor ip and count
server.use(async function(req, res, next) {
    try {
        console.log(`x-forwarded-for : ${req.headers['x-forwarded-for']}`)
        console.log(`remoteAddress : ${req.connection.remoteAddress}`)
        const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
        const result = await UTILS.verifyIp(ip)
        console.log(result)
        if (result == 'ok') {
            next()
        } else {
            res.send('over')
        }
    } catch (err) {
        console.log(err)
        res.send(new Error(err))
    }
})

server.post('/estimate', async function(req, res, next) {
    try {
        console.dir(req.body)
        const city = CITIES[parseInt(req.body.city) - 1]
        const district = DISTRICTS[parseInt(req.body.city) - 1][parseInt(req.body.district) - 1]
        const addr = city + district + req.body.address
        console.log(addr)
        const location = await UTILS.findCoordinateByAddress(addr)
        console.log(location)
        let avg = await UTILS.findPriceByCoordinateAndDistrict(location, district)
        const weight = UTILS.getWeight(req.body)
        avg = avg * weight * PLATEAU
        let estimatedPrice = parseFloat(req.body.area) * avg
        estimatedPrice = Math.floor(estimatedPrice / 10000)
        avg = Math.floor(avg / 10000)
        let possibleLoan = UTILS.getPossibleLoan(estimatedPrice, city, district)
        res.send({ estimatedPrice, possibleLoan, avg, location, addr })
    } catch(e) {
        console.log(e)
        res.send(new Error(e))        
    }
    return next()

})

server.post('/nearbyHouses', async function(req, res, next) {
    try {
        console.dir(req.body)
        const city = CITIES[parseInt(req.body.city) - 1]
        const district = DISTRICTS[parseInt(req.body.city) - 1][parseInt(req.body.district) - 1]
        const addr = city + district + req.body.address
        console.log(addr)
        const location = await UTILS.findCoordinateByAddress(addr)
        console.log(location)
        let avg = await UTILS.findPriceByCoordinateAndDistrict(location, district)
        const weight = UTILS.getWeight(req.body)
        avg = avg * weight * PLATEAU
        let estimatedPrice = parseFloat(req.body.area) * avg
        estimatedPrice = Math.floor(estimatedPrice / 10000)
        avg = Math.floor(avg / 10000)
        let possibleLoan = UTILS.getPossibleLoan(estimatedPrice, city, district)

        let nearbyHouses = await UTILS.getNearbyHouses(location, district)
        console.log('nearbyHouses count = ', nearbyHouses.length)
        res.send({ estimatedPrice, possibleLoan, avg, nearbyHouses, location, addr })
    } catch(e) {
        console.log(e)
        res.send(new Error(e))        
    }
    return next()

})

server.listen(8000, function() {
    console.log(`${server.name} listening at ${server.url}`)
})


