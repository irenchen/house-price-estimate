const config = require('../config')
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird');
mongoose.connect(config.dbUri, {
    useMongoClient: true
})

const { House, Visitor } = require('./models')

module.exports = {
    async getNearbyHousesByCoordinateAndDistrict(coordinate, district) {
        return new Promise((resolve, reject) => {
            House.find({
                location: {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [coordinate.lng, coordinate.lat]
                        },
                        $maxDistance: config.maxDistance // meter
                    }
                },
                district: district
            }, (err, docs) => {
                if(err) return reject(err)
                // console.log(docs)
                console.log(`nearby houses amount = ${docs.length}`)
                resolve(docs) 
            })
        })
    },
    async getNearbyHouses(coordinate, district, distance = config.maxDistance) {
        return new Promise((resolve, rejct) => {
            House.find({
                location: {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [coordinate.lng, coordinate.lat]
                        },
                        $maxDistance: distance // meter
                    }
                },
                district: district,
                memo: ''
            }, (err, docs) => {
                if(err) return reject(err)
                // console.log(docs)
                console.log(`nearby houses amount = ${docs.length}`)
                resolve(docs) 
            })
        })
    },
    async getVisitorByIp (ip) {
        return new Promise(async (resolve, reject) => {
            try {
                let visitor = await Visitor.findOne({ ip })
                if(!visitor) {
                    visitor = new Visitor({
                        ip: ip,
                        count: 0
                    })
                    await visitor.save()
                    resolve(visitor)
                } else {
                    resolve(visitor)
                }
            } catch (err) {
                reject(err)
            }
        })
    },
    async updateVisitorByIp (visitor) {
        return new Promise(async (resolve, reject) => {
            try {
                await Visitor.update({ ip: visitor.ip }, visitor)
                resolve('update ok')
            } catch (err) {
                console.log(err)
                reject(err)
            }
        })
    }
}