const mongoose = require('mongoose')
const Schema = mongoose.Schema

const houseSchema = Schema({
    serialNo: { type: String, unique: true },
    quarter: String,
    city: String,
    district: String,
    address: String,
    txDate: String,
    location: {
        type: {
            type: String,
            enum: "Point",
            default: "Point"
        },
        coordinates: {
            type: [Number],
            default: [0, 0]
        }
    },
    floor: Number,
    totalFloor: Number,
    houseType: String,
    houseCompleteDate: String,
    totalArea: Number,
    totalPrice: Number,
    pricePerArea: Number,
    management: { type: Boolean, default: false },
    parkingType: String,
    memo: String
})

houseSchema.index({ location: '2dsphere' })

const visitorSchema = Schema({
    ip: String,
    count: Number,
    visitTime: {
        type: Date,
        default: Date.now
    }
})

module.exports.House = mongoose.model('House', houseSchema)
module.exports.Visitor = mongoose.model('visitor', visitorSchema)
