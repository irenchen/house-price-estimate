const mongoose = require('mongoose')
mongoose.connect('mongodb://192.168.56.100:27017/lmarket')

const { House } = require('./models')



// House.findOne({}, (err, doc) => {
//     if(err) return console.log(err)
//     console.log(doc)
// })

// House.find({
//     location: {
//         $geoWithin: {
//             $centerSphere: [
//                 // [121.5149677, 25.0262886],
//                 [121.51, 25.02],
//                 5 / 3963.2 // radius of 5 miles
//             ]
//         }
//     }
// }, (err, docs) => {
//     if(err) return console.log(err)
//     console.log(docs)
// })

// House.find({
//     location: {
//         $nearSphere: {
//             $geometry: {
//                 type: "Point",
//                 coordinates: [121.5149677, 25.0262886]
//             },
//             $maxDistance: 500 // meter
//         }        
//     }
// }, (err, docs) => {
//     if(err) return console.log(err)
//     console.log(docs)    
// })

// House.find({
//     location: {
//         $near: {
//             $geometry: {
//                 type: "Point",
//                 coordinates: [121.5149677, 25.0262886]
//             },
//             $miinDistance: 1,
//             $maxDistance: 500 // meter
//         }        
//     }
// }, (err, docs) => {
//     if(err) return console.log(err)
//     console.log(docs)    
// })

House.find({
    location: {
        $geoWithin: {
            $geometry: {
                type: "Polygon",
                coordinates: [[[121.51, 25.02], [121.51, 25.03], [121.52, 25.03], [121.52, 25.02], [121.51, 25.02]]]
            }
        }        
    }
}, (err, docs) => {
    if(err) return console.log(err)
    console.log(docs)    
})
