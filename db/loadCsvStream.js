const fs = require('fs')
const csv = require('csv-parser')
const mongoose = require('mongoose')
const { House } = require('./models')

// mongoose.connect('mongodb://192.168.56.100:27017/lmarket')

const Rx = require('rxjs')

const source = Rx.Observable.create(function(obs) {

    fs.createReadStream('./doc/taipei.csv')
        .pipe(csv())
        .on('data', data => {
            obs.next(data)
        })
        .on('end', () => {
            console.log('parse csv file end.')
            obs.complete()
        })

})

source.take(4)
    .subscribe(console.log)

