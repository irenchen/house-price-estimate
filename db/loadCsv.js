const fs = require('fs')
const csv = require('csv-parser')
const EventEmitter = require('events')
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
const { House } = require('./models')
const config = require('../config')

mongoose.connect(config.dbUri, {
    useMongoClient: true
})

const city = process.argv[2] || '台北市'

var counter = 0
let realCounter = 0

fs.createReadStream(`./doc/${city}.csv`)
    .pipe(csv())
    .on('data', data => {
        counter++
        // if(counter >= 3 && counter <= 5) {
            if(data['期數'] && data['鄉鎮市區'] && data['土地區段位置/建物區段門牌']
               && data['交易年月日'] && data['GPS']) {
                realCounter++
                // console.log('quarter : ', data['期數'])
                // console.log('district : ', data['鄉鎮市區'])
                // console.log('address : ', data['土地區段位置/建物區段門牌'])
                // console.log('tx_date : ', data['交易年月日'])
                // console.log('location : ', data['GPS'])
                let loc = data['GPS'].split(',')
                let lat = parseFloat(loc[0].trim())
                let lng = parseFloat(loc[1].trim())
                // check invalid coordinate value
                if (isNaN(lat) || isNaN(lng)) return
                // console.log(data)
                let house = new House({
                    serialNo: data['編號'],
                    quarter: data['期數'],
                    city: city,
                    district: data['鄉鎮市區'],
                    address: data['土地區段位置/建物區段門牌'],
                    txDate: data['交易年月日'],
                    location: {
                        type: "Point",
                        coordinates: [lng, lat],
                    },
                    floor: floorMapping[data['移轉層次']],
                    totalFloor: floorMapping[data['總樓層數']],
                    houseType: data['建物型態'],
                    houseCompleteDate: data['建築完成年月'],
                    totalArea: data['建物移轉總面積(平方公尺)'],
                    pricePerArea: data['單價(元/平方公尺)'],
                    management: data['有無管理組織'],
                    totalPrice: data['總價(元)'],
                    parkingType: data['車位類別'],
                    memo: data['備註']
                })
                house.save(err => {
                    if(err) console.log(err)
                })
            }
        // }

    })
    .on('end', () => {
        console.log('parse csv file end. total count = ', counter)
        console.log('parse csv file end. total real count = ', realCounter)
    })


const floorMapping = {
    '一層': 1,
    '二層': 2,
    '三層': 3,
    '四層': 4,
    '五層': 5,
    '六層': 6,
    '七層': 7,
    '八層': 8,
    '九層': 9,
    '十層': 10,
    '十一層': 11,
    '十二層': 12,
    '十三層': 13,
    '十四層': 14,
    '十五層': 15,
    '十六層': 16,
    '十七層': 17,
    '十八層': 18,
    '十九層': 19,
    '二十層': 20,
    '二十一層': 21,
    '二十二層': 22,
    '二十三層': 23,
    '二十四層': 24,
    '二十五層': 25,
    '二十六層': 26,
    '二十七層': 27,
    '二十八層': 28,
    '二十九層': 29,
    '三十層': 30,
}
